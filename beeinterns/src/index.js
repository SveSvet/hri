import React from "react";
import ReactDOM from "react-dom";
import Layout from "./containers/App";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route  } from "react-router-dom";

import store from "./redux/store";

ReactDOM.render(
    <Router>
      <Provider store={store}>
      <Route path="/" component={Layout } exact />
      </Provider>
    </Router>,
  document.getElementById("root")
);
