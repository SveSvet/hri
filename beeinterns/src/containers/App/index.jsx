import React from "react";


// Styles
import './styles.css';


//Components
import {Heading, Menu, Result, Footer, Calculator} from '../../components'

const Layout = () => {
  const [visibleResult, setVisibleResult] = React.useState(false);
  const [visibleMenu, setVisibleMenu] = React.useState(false);

  const toggleVisibleMenu = () => {
    setVisibleMenu(true);
  };
  const hideMenu = () => {
    setVisibleMenu(visibleMenu);
  };
  const toggleVisibleResult = () => {
    setVisibleResult(!visibleResult);
  };
  const hideResult = () => {
    setVisibleResult(visibleResult);
  };
  return (
    <div>
      <Heading />
      <Calculator hideMenu={hideMenu} hideResult={hideResult} toggleVisibleResult={toggleVisibleResult} />
      {visibleResult && <Result toggleVisibleMenu={toggleVisibleMenu} />}
      {visibleMenu && <Menu/>}
      <Footer/>
    </div>
  );
};

export default Layout;
