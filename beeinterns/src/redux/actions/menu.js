import axios from "axios";

export const fetchMenu = (bmi) => (dispatch) => {
  axios.get(`http://localhost:3001/menu?bmi=${bmi}`).then(({ data }) => {
    console.log(data)
    dispatch(setMenu(data));
  });
};
export const setMenu = (items) => ({
  type: "SET_MENU",
  payload: items,
});

