import React from "react";
import "./index.css";

//images
import fb from "../../assets/img/facebook.svg";
import twitter from "../../assets/img/twitter.svg";
import vk from "../../assets/img/vk.svg";

const HeadBanner = () => {
    const social = [fb, twitter, vk];

    return (
        <div className="banner">
            <h1 className="title">Healthy mind</h1>
            <p className="desc">
                Индекс массы тела — величина, позволяющая оценить
                степень соответствия массы человека и его роста
                и тем самым косвенно судить о том,
                является ли масса недостаточной,
                нормальной или избыточной.
            </p>
            <div className="social">
                {social.map((item) => <a href="#" key={item}><img src={item} key={item} alt=""/></a>)}
            </div>
        </div>
    )
};


export default HeadBanner;
